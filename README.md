datalife_sale_cost_apply_purchase
=================================

The sale_cost_apply_purchase module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-sale_cost_apply_purchase/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-sale_cost_apply_purchase)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
