# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond.pool import PoolMeta, Pool
from trytond.model import fields
from trytond.pyson import Eval
from trytond.transaction import Transaction
from trytond.exceptions import UserError
from trytond.i18n import gettext
from sql.operators import Concat
from functools import partial
from itertools import groupby


class SaleCost(metaclass=PoolMeta):
    __name__ = 'sale.cost'

    purchase = fields.Function(
        fields.Many2One('purchase.purchase', 'Purchase', readonly=True,
        states={
            'invisible': Eval('apply_method', '') != 'purchase'
        }, depends=['apply_method']),
        'get_purchase', searcher='search_purchase')
    purchase_lines = fields.One2Many('purchase.line', 'origin',
     'Purchase lines',
        states={
            'invisible': Eval('apply_method', '') != 'purchase'
        }, depends=['apply_method'])

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls.apply_method.selection.append(('purchase', 'Purchase'))
        cls.invoice_party.states['invisible'] &= (
            Eval('apply_method') != 'purchase')

    @classmethod
    def __register__(cls, module_name):
        table = cls.__table_handler__(module_name)
        Purchase_lines = Pool().get('purchase.line')
        cursor = Transaction().connection.cursor()
        sql_table = cls.__table__()
        purchase_line = Purchase_lines.__table__()

        purchase_exists = table.column_exist('purchase')

        super(SaleCost, cls).__register__(module_name)

        if purchase_exists:
            cursor.execute(*purchase_line.join(sql_table,
                    condition=purchase_line.purchase == sql_table.purchase
                    ).select(
                        purchase_line.id,
                        sql_table.id,
                        where=(purchase_line.type == 'line'))
            )

            for pline_id, cost_id in cursor.fetchall():
                cursor.execute(*purchase_line.update(
                    columns=[purchase_line.origin],
                    values=[Concat(cls.__name__ + ',', cost_id)],
                    where=purchase_line.id == pline_id
                    ))

            table.drop_column('purchase')

    def get_purchase(self, name=None):
        if self.purchase_lines:
            return self.purchase_lines[0].purchase.id

    @ classmethod
    def search_purchase(cls, name, clause):
        return [('purchase_lines.' + clause[0],) + tuple(clause[1:])]

    @classmethod
    def _apply_method(cls, apply_method, costs):
        Purchase = Pool().get('purchase.purchase')

        if apply_method != 'purchase':
            return super()._apply_method(apply_method, costs)

        cost_keyfunc = partial(cls._get_purchase_keygroup)
        sorted_cost_lines = sorted([c for c in costs if not c.purchase_lines],
            key=cost_keyfunc)
        purchases = {}

        for key, grouped_lines in groupby(sorted_cost_lines, key=cost_keyfunc):
            purchase = Purchase(**dict(key))
            purchase.on_change_party()
            lines = []

            group_lines = list(grouped_lines)
            descriptions = set()
            for cost in group_lines:
                line = cost._get_purchase_line(purchase)
                if line:
                    lines.append(line)
                if cost.description is not None:
                    descriptions.add(cost.description.strip().lower())

            if len(descriptions) == 1:
                purchase.description = cost.description
            if lines:
                purchase.lines = lines
                purchases.setdefault(cost.type_.purchase_state,
                    []).append(purchase)

        to_save, to_process = [], []
        for state, _purchases in purchases.items():
            to_save.extend(_purchases)
            if state == 'processing':
                to_process.extend(_purchases)
        if to_save:
            Purchase.save(to_save)
            Purchase.quote(to_save)
        if to_process:
            Purchase.confirm(to_process)
            Purchase.proceed(to_process)

        return []

    def _get_purchase_line(self, purchase):
        pool = Pool()
        PurchaseLine = pool.get('purchase.line')

        if not self.amount or self.purchase_lines:
            return

        line = PurchaseLine()
        line.purchase = purchase
        line.type = 'line'
        line.product = self.type_.product
        line.on_change_product()
        line.quantity = self.quantity
        line.on_change_quantity()
        digits = PurchaseLine.unit_price.digits[1]
        line.unit_price = (self.amount / Decimal(str(self.quantity))
            ).quantize(Decimal(10) ** -Decimal(digits))
        line.amount = line.on_change_with_amount()
        line.origin = self
        return line

    @classmethod
    def _get_purchase_keygroup(cls, cost):
        if not cost.invoice_party:
            raise UserError(gettext('document_cost_apply_invoice.'
                'msg_document_cost_apply_invoice_invoice_no_party',
                type_=cost.type_.rec_name,
                cost=cost.document.rec_name))
        return [
            ('company', cost.document.company),
            ('party', cost.invoice_party),
            ('purchase_date', cost.sale.sale_date)]

    @classmethod
    def _unapply_method(cls, apply_method, costs):
        pool = Pool()
        Purchase = pool.get('purchase.purchase')

        if apply_method != 'purchase':
            super(SaleCost, cls)._unapply_method(apply_method, costs)

        purchases = Purchase.search([
            ('id', 'in', [c.purchase.id for c in costs if c.purchase])])
        if purchases:
            Purchase.delete(purchases)

    @fields.depends('purchase_lines')
    def get_compute_formula(self, name=None):
        return bool(super().get_compute_formula(name)
            and not self.purchase_lines)

    def get_appliable(self, name=None):
        res = super().get_appliable(name)
        if self.apply_method == 'purchase':
            res &= not bool(self.purchase_lines)
        return res

    def get_applied(self, name=None):
        return super().get_applied(name) or (
            self.apply_method == 'purchase'
            and bool(self.purchase_lines))

    @classmethod
    def search_applied(cls, name, clause):
        domain = super().search_applied(name, clause)

        join_operator, operator1, operator2 = 'AND', '=', '!='
        if ((clause[1] == '!=' and clause[2])
                or (clause[1] == '=' and not clause[2])):
            join_operator, operator1, operator2 = 'OR', '!=', '='

        domain.append([join_operator,
            ('apply_method', operator1, 'purchase'),
            ('purchase_lines', operator2, None)])

        return domain

    @property
    def non_explodable(self):
        return super().non_explodable or bool(self.purchase_lines)


class CostType(metaclass=PoolMeta):
    __name__ = 'sale.cost.type'

    purchase_state = fields.Selection([
            (None, ''),
            ('quotation', 'Quotation'),
            ('processing', 'Processing')
        ], 'Purchase state',
        states={
            'invisible': Eval('apply_method') != 'purchase',
            'required': Eval('apply_method') == 'purchase'
        },
        depends=['apply_method'])

    @fields.depends('apply_method')
    def on_change_with_purchase_state(self, name=None):
        if self.apply_method == 'purchase':
            return 'quotation'
        else:
            return None

    @classmethod
    def __setup__(cls):
        super(CostType, cls).__setup__()
        cls.apply_method.selection.append(('purchase', 'Purchase'))
